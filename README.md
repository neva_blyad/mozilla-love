# NAME

mozilla+love

# VERSION

0.07

# DESCRIPTION

Certificates for Mozilla Firefox and Thunderbird to access lovecry.pt Web and
mail respectively.

These certificates are also to be used by various software with HTTPS support
such as GnuPG.

**UPD:**

These self-signed SSL certificates are not used anymore.

Do not use them!

Now I use certificate signed by Let's Encrypt.

# AUTHORS

    НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                    <neva_blyad@lovecri.es>
    Invisible Light
