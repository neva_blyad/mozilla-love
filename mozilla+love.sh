#!/bin/sh
#
#    mozilla+love.sh
#    Copyright (C) 2019-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
#                                            <neva_blyad@lovecri.es>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

# https://stackoverflow.com/questions/1435000/programmatically-install-certificate-into-mozilla

certificateFile="lovecry.pt.crt"
certificateName="LoveCry.pt"

for certDB in $(find ~/.mozilla/ -name "cert9.db")
do
  certDir=$(dirname ${certDB});
  certutil -A -n "${certificateName}" -t ",," -i ${certificateFile} -d ${certDir}

  echo "www.lovecry.pt:443	OID.2.16.840.1.101.3.4.2.1	05:3F:0C:D0:3C:DF:7B:58:0C:32:EB:7E:A0:D3:E5:22:98:7A:D3:9C:BA:2D:EF:9F:E7:FB:E5:08:F6:6F:8F:9C	U	AAAAAAAAAAAAAAAJAAAAewDLuowtVL3WzzB5MQswCQYDVQQGEwJSVTERMA8GA1UEBxMITmV2b2dyYWQxGjAYBgNVBAoTEUVjY2xlc2lhIFNwaXJpdHVtMRUwEwYDVQQDFAwqLmxvdmVjcnkucHQxJDAiBgkqhkiG9w0BCQEWFW5ldmFfYmx5YWRAbG92ZWNyeS5wdA==" >> \
    ${certDir}/cert_override.txt
  echo "www.lovecri.es:443	OID.2.16.840.1.101.3.4.2.1	05:3F:0C:D0:3C:DF:7B:58:0C:32:EB:7E:A0:D3:E5:22:98:7A:D3:9C:BA:2D:EF:9F:E7:FB:E5:08:F6:6F:8F:9C	U	AAAAAAAAAAAAAAAJAAAAewDLuowtVL3WzzB5MQswCQYDVQQGEwJSVTERMA8GA1UEBxMITmV2b2dyYWQxGjAYBgNVBAoTEUVjY2xlc2lhIFNwaXJpdHVtMRUwEwYDVQQDFAwqLmxvdmVjcnkucHQxJDAiBgkqhkiG9w0BCQEWFW5ldmFfYmx5YWRAbG92ZWNyeS5wdA==" >> \
    ${certDir}/cert_override.txt
  echo "www.gromkosa.ru:443	OID.2.16.840.1.101.3.4.2.1	05:3F:0C:D0:3C:DF:7B:58:0C:32:EB:7E:A0:D3:E5:22:98:7A:D3:9C:BA:2D:EF:9F:E7:FB:E5:08:F6:6F:8F:9C	U	AAAAAAAAAAAAAAAJAAAAewDLuowtVL3WzzB5MQswCQYDVQQGEwJSVTERMA8GA1UEBxMITmV2b2dyYWQxGjAYBgNVBAoTEUVjY2xlc2lhIFNwaXJpdHVtMRUwEwYDVQQDFAwqLmxvdmVjcnkucHQxJDAiBgkqhkiG9w0BCQEWFW5ldmFfYmx5YWRAbG92ZWNyeS5wdA==" >> \
    ${certDir}/cert_override.txt
  echo "keyring.lovecry.pt:443	OID.2.16.840.1.101.3.4.2.1	05:3F:0C:D0:3C:DF:7B:58:0C:32:EB:7E:A0:D3:E5:22:98:7A:D3:9C:BA:2D:EF:9F:E7:FB:E5:08:F6:6F:8F:9C	U	AAAAAAAAAAAAAAAJAAAAewDLuowtVL3WzzB5MQswCQYDVQQGEwJSVTERMA8GA1UEBxMITmV2b2dyYWQxGjAYBgNVBAoTEUVjY2xlc2lhIFNwaXJpdHVtMRUwEwYDVQQDFAwqLmxvdmVjcnkucHQxJDAiBgkqhkiG9w0BCQEWFW5ldmFfYmx5YWRAbG92ZWNyeS5wdA==" >> \
    ${certDir}/cert_override.txt
  echo "keyring.lovecri.es:443	OID.2.16.840.1.101.3.4.2.1	05:3F:0C:D0:3C:DF:7B:58:0C:32:EB:7E:A0:D3:E5:22:98:7A:D3:9C:BA:2D:EF:9F:E7:FB:E5:08:F6:6F:8F:9C	U	AAAAAAAAAAAAAAAJAAAAewDLuowtVL3WzzB5MQswCQYDVQQGEwJSVTERMA8GA1UEBxMITmV2b2dyYWQxGjAYBgNVBAoTEUVjY2xlc2lhIFNwaXJpdHVtMRUwEwYDVQQDFAwqLmxvdmVjcnkucHQxJDAiBgkqhkiG9w0BCQEWFW5ldmFfYmx5YWRAbG92ZWNyeS5wdA==" >> \
    ${certDir}/cert_override.txt
done

for certDB in $(find ~/.thunderbird/ -name "cert9.db")
do
  certDir=$(dirname ${certDB});
  certutil -A -n "${certificateName}" -t ",," -i ${certificateFile} -d ${certDir}

  echo "imap.lovecry.pt:143	OID.2.16.840.1.101.3.4.2.1	05:3F:0C:D0:3C:DF:7B:58:0C:32:EB:7E:A0:D3:E5:22:98:7A:D3:9C:BA:2D:EF:9F:E7:FB:E5:08:F6:6F:8F:9C	U	AAAAAAAAAAAAAAAJAAAAewDLuowtVL3WzzB5MQswCQYDVQQGEwJSVTERMA8GA1UEBxMITmV2b2dyYWQxGjAYBgNVBAoTEUVjY2xlc2lhIFNwaXJpdHVtMRUwEwYDVQQDFAwqLmxvdmVjcnkucHQxJDAiBgkqhkiG9w0BCQEWFW5ldmFfYmx5YWRAbG92ZWNyeS5wdA==" >> \
    ${certDir}/cert_override.txt
  echo "smtp.lovecry.pt:587	OID.2.16.840.1.101.3.4.2.1	05:3F:0C:D0:3C:DF:7B:58:0C:32:EB:7E:A0:D3:E5:22:98:7A:D3:9C:BA:2D:EF:9F:E7:FB:E5:08:F6:6F:8F:9C	U	AAAAAAAAAAAAAAAJAAAAewDLuowtVL3WzzB5MQswCQYDVQQGEwJSVTERMA8GA1UEBxMITmV2b2dyYWQxGjAYBgNVBAoTEUVjY2xlc2lhIFNwaXJpdHVtMRUwEwYDVQQDFAwqLmxvdmVjcnkucHQxJDAiBgkqhkiG9w0BCQEWFW5ldmFfYmx5YWRAbG92ZWNyeS5wdA==" >> \
    ${certDir}/cert_override.txt
  echo "imap.lovecri.es:143	OID.2.16.840.1.101.3.4.2.1	05:3F:0C:D0:3C:DF:7B:58:0C:32:EB:7E:A0:D3:E5:22:98:7A:D3:9C:BA:2D:EF:9F:E7:FB:E5:08:F6:6F:8F:9C	U	AAAAAAAAAAAAAAAJAAAAewDLuowtVL3WzzB5MQswCQYDVQQGEwJSVTERMA8GA1UEBxMITmV2b2dyYWQxGjAYBgNVBAoTEUVjY2xlc2lhIFNwaXJpdHVtMRUwEwYDVQQDFAwqLmxvdmVjcnkucHQxJDAiBgkqhkiG9w0BCQEWFW5ldmFfYmx5YWRAbG92ZWNyeS5wdA==" >> \
    ${certDir}/cert_override.txt
  echo "smtp.lovecri.es:587	OID.2.16.840.1.101.3.4.2.1	05:3F:0C:D0:3C:DF:7B:58:0C:32:EB:7E:A0:D3:E5:22:98:7A:D3:9C:BA:2D:EF:9F:E7:FB:E5:08:F6:6F:8F:9C	U	AAAAAAAAAAAAAAAJAAAAewDLuowtVL3WzzB5MQswCQYDVQQGEwJSVTERMA8GA1UEBxMITmV2b2dyYWQxGjAYBgNVBAoTEUVjY2xlc2lhIFNwaXJpdHVtMRUwEwYDVQQDFAwqLmxvdmVjcnkucHQxJDAiBgkqhkiG9w0BCQEWFW5ldmFfYmx5YWRAbG92ZWNyeS5wdA==" >> \
    ${certDir}/cert_override.txt
  echo "imap.gromkosa.ru:143	OID.2.16.840.1.101.3.4.2.1	05:3F:0C:D0:3C:DF:7B:58:0C:32:EB:7E:A0:D3:E5:22:98:7A:D3:9C:BA:2D:EF:9F:E7:FB:E5:08:F6:6F:8F:9C	U	AAAAAAAAAAAAAAAJAAAAewDLuowtVL3WzzB5MQswCQYDVQQGEwJSVTERMA8GA1UEBxMITmV2b2dyYWQxGjAYBgNVBAoTEUVjY2xlc2lhIFNwaXJpdHVtMRUwEwYDVQQDFAwqLmxvdmVjcnkucHQxJDAiBgkqhkiG9w0BCQEWFW5ldmFfYmx5YWRAbG92ZWNyeS5wdA==" >> \
    ${certDir}/cert_override.txt
  echo "smtp.gromkosa.ru:587	OID.2.16.840.1.101.3.4.2.1	05:3F:0C:D0:3C:DF:7B:58:0C:32:EB:7E:A0:D3:E5:22:98:7A:D3:9C:BA:2D:EF:9F:E7:FB:E5:08:F6:6F:8F:9C	U	AAAAAAAAAAAAAAAJAAAAewDLuowtVL3WzzB5MQswCQYDVQQGEwJSVTERMA8GA1UEBxMITmV2b2dyYWQxGjAYBgNVBAoTEUVjY2xlc2lhIFNwaXJpdHVtMRUwEwYDVQQDFAwqLmxvdmVjcnkucHQxJDAiBgkqhkiG9w0BCQEWFW5ldmFfYmx5YWRAbG92ZWNyeS5wdA==" >> \
    ${certDir}/cert_override.txt
done
